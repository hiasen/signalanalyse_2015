import numpy as np

# Making the time vector to use for all the pulses
sample_frequency = 1e4 # Hz
time_step = 1/sample_frequency # seconds
time_vector_length = 3 # seconds
time = np.arange(0, time_vector_length, time_step) # seconds

# Setting some parameters that will be used for
# both the rectangular pulse and the triangular pulse
pulse_length = 10e-3 # s
amplitude = 1
centered_around = 1 # s

# Finding the positions of the pulse
pulse_middle = centered_around * sample_frequency
num_samples_in_pulse = pulse_length * sample_frequency
num_samples_in_half_pulse_width = pulse_length/2 * sample_frequency

pulse_start = pulse_middle - num_samples_in_pulse / 2
pulse_end = pulse_middle + num_samples_in_pulse / 2

# Create the rectangular pulse using the previously created timevector
rectangular_pulse = np.zeros(time.size)
rectangular_pulse[pulse_start:pulse_end] = 1

# Here we are using the same parameters as for the rectangular pulse
triangular_pulse = np.zeros(time.size)
triangular_pulse[pulse_start:pulse_middle] = np.linspace(0, amplitude, pulse_middle-pulse_start)
triangular_pulse[pulse_middle:pulse_end] = np.linspace(amplitude, 0, pulse_end - pulse_middle)


gaussian_pulse = np.zeros(time.size)
gauss_width = 3e-3 # seconds
gauss_width_samples = gauss_width * sample_frequency

gauss_pulse_interval = slice(pulse_middle-gauss_width_samples/2, pulse_middle+gauss_width_samples/2)
x = np.linspace(0, gauss_width, gauss_width_samples)
gaussian_pulse[gauss_pulse_interval] = np.exp(-((x - gauss_width/2)/(gauss_width/6))**2/2)


# Initialize empty vector of same size as time vector
sequence_of_rect_pulses = np.zeros(time.size)

# Parameters of pulses in real units
num_pulses = 11
start_time = 1 # s
time_period = 10e-3 # s
pulse_width = 1e-3 # s (1ms)

# Sample information
width = int(pulse_width * sample_frequency)
start = int(start_time * sample_frequency)
period = int(time_period * sample_frequency)

start_points = range(start, start + period*num_pulses, period)
for s in start_points:
    sequence_of_rect_pulses[s:s+width] = 1